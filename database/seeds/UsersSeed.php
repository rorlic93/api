<?php
/**
 * Created by PhpStorm.
 * User: Rastimir
 * Date: 07.05.2017.
 * Time: 18:12
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Maker;
use App\User;

class UsersSeed extends Seeder{

    public function run(){
        User::create([
            'email' => 'orlicrastimir@gmail.com',
            'password' => Hash::make('RO')
        ]);
    }

}