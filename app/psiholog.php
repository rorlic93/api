<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class psiholog extends Model
{
    //

    protected $table = 'psiholog';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'ime', 'prezime'];

    public function nalaz(){
        return $this->hasMany('App\nalaz');
    }
}
