<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class odgojiteljica extends Model
{
    //

    protected $table = 'odgojiteljica';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'ime', 'prezime', 'skupina_id'];

    public function skupina(){
        return $this->belongsTo('App\skupina');
    }
}
