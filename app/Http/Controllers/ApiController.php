<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\dijete;
use App\dijete_poremecaj;
use App\poremecaj;
use App\nalaz;
use App\odgojiteljica;
use App\psiholog;
use App\vrtic;

class ApiController extends Controller
{

    public function __construct(){
        $this->middleware('auth.basic');
    }

    public function index()
    {
        $djeca = dijete::all();
        $dijete_poremecaj = dijete_poremecaj::all();
        $poremecaji = poremecaj::all();
        $nalazi = nalaz::all();
        $odgojiteljice = odgojiteljica::all();
        $psiholog = psiholog::all();
        $vrtic = vrtic::all();

//        return response()->json(['data' => 'in index function']);

        return response()->json(['djeca' => $djeca, 'poremecaji' => $poremecaji, 'dijete_poremecaj' => $dijete_poremecaj, "nalazi" => $nalazi, 'odgojiteljice' => $odgojiteljice, 'psiholozi' => $psiholog, 'vrtici' => $vrtic]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
