<?php
/**
 * Created by PhpStorm.
 * User: Rastimir
 * Date: 07.05.2017.
 * Time: 21:51
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\Middleware;

class OnceAuth implements Middleware
{

    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        $fails = $this->auth->onceBasic();
        if($fails){
            return response()->json(['message' => 'You have not acess to this kind of request', 'code' => 401], 401);
        }
        return $next($request);
    }
}
